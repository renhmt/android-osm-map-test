#### config
#### settings.gradle (repositories|dependencyResolutionManagement) -> maven { url "https://jitpack.io" }
#### build.gradle (app|dependencies) -> osmdroid and osmbonuspack
```kotlin
private lateinit var map: MapView
private lateinit var mLocationOverlay: MyLocationNewOverlay
private var startPoint: GeoPoint = GeoPoint(0.0, 0.0)
```
#### fix path sdcard
```kotlin
Configuration.getInstance().osmdroidBasePath = filesDir
```
#### set view tile
```kotlin
val tileSource: ITileSource = XYTileSource(
    "HOT", 1, 20, 256, ".png", arrayOf(
        "https://map.yousite.com/osm/",
    ), "© OpenStreetMap"
)
map.setTileSource(tileSource)
// or 
map.setTileSource(TileSourceFactory.MAPNIK)
```
#### zoom to default point
```kotlin
map.controller.animateTo(startPoint)
map.controller.zoomTo(17, 1)
```
#### enable finger pinch
```kotlin
map.setMultiTouchControls(true) // zoom
map.overlays.add( RotationGestureOverlay(map)) // rotate
map.setBuiltInZoomControls(false) // disable btn +-
```
#### request you location
```kotlin
registerForActivityResult(ActivityResultContracts.RequestPermission()) {
    val mGpsMyLocationProvider = GpsMyLocationProvider(this)
    mLocationOverlay = MyLocationNewOverlay(mGpsMyLocationProvider, map)
    mLocationOverlay.enableMyLocation()
    mLocationOverlay.enableFollowLocation()
    map.overlays.add(mLocationOverlay)
    map.controller.animateTo(mLocationOverlay.myLocation)
    map.controller.zoomTo(17, 1)
}.launch(Manifest.permission.ACCESS_FINE_LOCATION)
```
#### Road
```kotlin
CoroutineScope(Dispatchers.IO).launch {
    // load road class
    val roadManager = OSRMRoadManager(this@MainActivity, System.getProperty("http.agent"))
//                roadManager.setService("https://route.yousite.com/") no
    // value of point
    val waypoints = ArrayList<GeoPoint>()
    // set first point
    waypoints.add(mLocationOverlay.myLocation?:startPoint)
    // set second point
    waypoints.add(p)
    // get road from OSRM site
    val road = roadManager.getRoad(waypoints)
    // parse road to poly line
    val roadOverlay: Polyline = RoadManager.buildRoadOverlay(road)
    // display road and
    withContext(Dispatchers.Main){
        map.overlays.add(roadOverlay);
        map.invalidate()
    }
}
```

#### add marker (optional)
```kotlin
val marker = Marker(map)
// set marker point
marker.position = GeoPoint(p.latitude, p.longitude)
// set marker title
marker.title = "Test"
// set anchor to image
marker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
// get icon (resources)
val d = ContextCompat.getDrawable(this@MainActivity, R.drawable.icons_car);
// set custom marker icon
marker.icon = d
// click marker listener
marker.setOnMarkerClickListener { marker, mapView ->
    // show dialog info of point
    AlertDialog.Builder(this@MainActivity)
        .setTitle(marker.title)
        .setMessage(marker.position.toString())
        .show()
    return@setOnMarkerClickListener true
}
// add marker to map
map.overlays.add(marker)
```

[merge-requests](https://docs.gitlab.com/ee/user/project/merge_requests/#view-merge-requests-for-a-project)