package com.example.myapplicationmap

import android.Manifest
import android.os.Bundle
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.osmdroid.bonuspack.routing.OSRMRoadManager
import org.osmdroid.bonuspack.routing.RoadManager
import org.osmdroid.config.Configuration
import org.osmdroid.events.MapEventsReceiver
import org.osmdroid.tileprovider.tilesource.ITileSource
import org.osmdroid.tileprovider.tilesource.XYTileSource
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.MapView
import org.osmdroid.views.overlay.MapEventsOverlay
import org.osmdroid.views.overlay.Marker
import org.osmdroid.views.overlay.Polyline
import org.osmdroid.views.overlay.gestures.RotationGestureOverlay
import org.osmdroid.views.overlay.mylocation.GpsMyLocationProvider
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay


class MainActivity : AppCompatActivity(), MapEventsReceiver {

    // config
    // settings.gradle (dependencyResolutionManagement|repositories) -> maven { url "https://jitpack.io" }
    // build.gradle (app|dependencies) -> osmdroid and osmbonuspack

    private lateinit var map: MapView
    private lateinit var mLocationOverlay: MyLocationNewOverlay
    private var startPoint: GeoPoint = GeoPoint(54.19693, 45.173141)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // fix path sdcard
        Configuration.getInstance().osmdroidBasePath = filesDir
        // load view
        setContentView(R.layout.activity_main)
        // find node
        map = findViewById<MapView>(R.id.mapView)

        map.setMultiTouchControls(true) // zoom
        map.overlays.add( RotationGestureOverlay(map)) // rotate
        map.setBuiltInZoomControls(false) // disable btn +-
        // get tile to server
        val tileSource: ITileSource = XYTileSource(
            "HOT", 1, 20, 256, ".png", arrayOf(
                "https://map.yousite.com/osm/",
            ), "© OpenStreetMap"
        )
        // set view tile
        map.setTileSource(tileSource)

        // zoom to default point
        map.controller.animateTo(startPoint)
        map.controller.zoomTo(17, 1)

        // request you location
        registerForActivityResult(ActivityResultContracts.RequestPermission()) {
            val mGpsMyLocationProvider = GpsMyLocationProvider(this)
            mLocationOverlay = MyLocationNewOverlay(mGpsMyLocationProvider, map)
            mLocationOverlay.enableMyLocation()
            mLocationOverlay.enableFollowLocation()
            map.overlays.add(mLocationOverlay)
            map.controller.animateTo(mLocationOverlay.myLocation)
            map.controller.zoomTo(17, 1)
        }.launch(Manifest.permission.ACCESS_FINE_LOCATION)

        // add click map listener
        map.overlays.add(MapEventsOverlay(this))
    }

    // click to map listener
    override fun singleTapConfirmedHelper(p: GeoPoint?): Boolean {
        if (p != null) {
            CoroutineScope(Dispatchers.IO).launch {
                // load road class
                val roadManager = OSRMRoadManager(this@MainActivity, System.getProperty("http.agent"))
//                roadManager.setService("https://route.yousite.com/") no
                // value of point
                val waypoints = ArrayList<GeoPoint>()
                // set first point
                waypoints.add(mLocationOverlay.myLocation?:startPoint)
                // set second point
                waypoints.add(p)
                // get road from OSRM site
                val road = roadManager.getRoad(waypoints)
                // parse road to poly line
                val roadOverlay: Polyline = RoadManager.buildRoadOverlay(road)
                // display road and
                withContext(Dispatchers.Main){
                    map.overlays.add(roadOverlay);
                    map.invalidate()

                    // add marker (optional)
                    val marker = Marker(map)
                    // set marker point
                    marker.position = GeoPoint(p.latitude, p.longitude)
                    // set marker title
                    marker.title = "Test"
                    // set anchor to image
                    marker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
                    // get icon (resources)
                    val d = ContextCompat.getDrawable(this@MainActivity, R.drawable.icons_car);
                    // set custom marker icon
                    marker.icon = d
                    // click marker listener
                    marker.setOnMarkerClickListener { marker, mapView ->
                        // show dialog info of point
                        AlertDialog.Builder(this@MainActivity)
                            .setTitle(marker.title)
                            .setMessage(marker.position.toString())
                            .show()
                        return@setOnMarkerClickListener true
                    }
                    // add marker to map
                    map.overlays.add(marker)
                }
            }

        }
        return true
    }

    override fun longPressHelper(p: GeoPoint?): Boolean {
        return true
    }
}